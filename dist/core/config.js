'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

exports.default = getConfig;

var _fs = require('fs');

var _fs2 = _interopRequireDefault(_fs);

var _path = require('path');

var _path2 = _interopRequireDefault(_path);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function getConfigPath() {
  var basePath = process.cwd();
  var testsCount = 0;
  var shouldContinue = true;

  while (shouldContinue) {
    try {
      if (testsCount > 0) {
        basePath = _path2.default.join(basePath, '../');
      }

      var testPath = _path2.default.join(basePath, '.dot.json');

      _fs2.default.statSync(testPath);

      return testPath;
    } catch (err) {
      if (testsCount < 5) {
        testsCount++;
      } else {
        shouldContinue = false;
        throw new Error('Please create a ".dot.json" file on project root');
      }
    }
  }
}

function getConfig() {
  return new Promise(function (resolve, reject) {
    try {
      var configPath = getConfigPath();
      var config = require(configPath);

      if (!config.options) {
        config.options = {};
      }

      resolve(_extends({ projectRoot: _path2.default.dirname(configPath) }, config));
    } catch (err) {
      reject(err);
    }
  });
}