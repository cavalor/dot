'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = handleError;

var _print = require('./print');

var _print2 = _interopRequireDefault(_print);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function handleError(err) {
  if (err.message) {
    _print2.default.error(err.message);
    _print2.default.stack(err.stack);
  } else {
    _print2.default.error(err);
  }
  _print2.default.separator('-');
  process.exit();
}