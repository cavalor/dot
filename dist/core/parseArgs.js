'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _slicedToArray = function () { function sliceIterator(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"]) _i["return"](); } finally { if (_d) throw _e; } } return _arr; } return function (arr, i) { if (Array.isArray(arr)) { return arr; } else if (Symbol.iterator in Object(arr)) { return sliceIterator(arr, i); } else { throw new TypeError("Invalid attempt to destructure non-iterable instance"); } }; }();

exports.default = parseArgs;

var _yargs = require('yargs');

function parseArgs() {
  var args = _yargs.argv['_'];

  if (args.length === 0) {
    return { args: [] };
  }

  if (args[0].indexOf(':') === -1) {
    var _args = _slicedToArray(args, 2),
        _plugin = _args[0],
        _command = _args[1];

    return { plugin: _plugin, command: _command, args: args.slice(2) };
  }

  var _args$0$split = args[0].split(':'),
      _args$0$split2 = _slicedToArray(_args$0$split, 2),
      plugin = _args$0$split2[0],
      command = _args$0$split2[1];

  return { plugin: plugin, command: command, args: args.slice(1) };
}