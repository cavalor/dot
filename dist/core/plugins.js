'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getPlugin = getPlugin;
exports.getPlugins = getPlugins;
exports.printPluginCommands = printPluginCommands;
exports.handleRequest = handleRequest;

var _fs = require('fs');

var _fs2 = _interopRequireDefault(_fs);

var _path = require('path');

var _path2 = _interopRequireDefault(_path);

var _print = require('./print');

var _print2 = _interopRequireDefault(_print);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } else { return Array.from(arr); } }

var pluginDir = _path2.default.join(__dirname, '../plugins');

/**
 * Import plugin definition file
 * @param pluginName
 */
function getPlugin(pluginName) {
  var plugin = require(_path2.default.join(pluginDir, pluginName)).default;

  plugin.name = pluginName;

  return plugin;
}

/**
 * Get all available plugins then filter them with the list given in .dot.json
 * @param cli
 * @returns {Promise}
 */
function getPlugins(cli) {
  var config = cli.config;


  return new Promise(function (resolve, reject) {
    if (!config) {
      return reject(Error('"Config" should be provided by cli interface'));
    }

    if (!config.plugins) {
      return reject(Error('You should provide a "plugins" entry in .dot.json config'));
    }

    // Read plugin directory
    _fs2.default.readdir(pluginDir, function (err, availablePlugins) {
      if (err) {
        return reject(err);
      }

      var enabledPlugins = availablePlugins.filter(function (f) {
        return config.plugins.indexOf(f) !== -1;
      });

      resolve(enabledPlugins.map(getPlugin));
    });
  });
}

/**
 * List all commands for a given plugin
 * @param plugin {String}
 */
function printPluginCommands(plugin) {
  var commands = plugin.commands,
      name = plugin.name;

  var namesLength = commands.map(function (c) {
    return c.name.length;
  });
  var maxLength = Math.max.apply(Math, _toConsumableArray(namesLength));

  var output = _print2.default.format(name, { ucFirst: true, bold: true });

  var _iteratorNormalCompletion = true;
  var _didIteratorError = false;
  var _iteratorError = undefined;

  try {
    for (var _iterator = commands[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
      var command = _step.value;

      output = output.concat('\n', _print2.default.format(' * '.concat(command.name), { bold: true }));
      output = output.concat(' '.repeat(maxLength - command.name.length));
      output = output.concat(_print2.default.format(' | '.concat(command.description), { color: 'grey' }));
    }
  } catch (err) {
    _didIteratorError = true;
    _iteratorError = err;
  } finally {
    try {
      if (!_iteratorNormalCompletion && _iterator.return) {
        _iterator.return();
      }
    } finally {
      if (_didIteratorError) {
        throw _iteratorError;
      }
    }
  }

  console.log(output, '\n');
}

/**
 * Require handler related to the current request information
 * @param cli {Object}
 */
function handleRequest(cli) {
  var _cli$request = cli.request,
      plugin = _cli$request.plugin,
      command = _cli$request.command;


  _print2.default.header(_print2.default.ucFirst(plugin.name) + '::' + command.name);

  var cmd = require(_path2.default.join(pluginDir, plugin.name, 'commands', command.name)).default;
  return cmd(cli);
}