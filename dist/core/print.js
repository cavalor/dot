'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.styles = undefined;

var _chalk = require('chalk');

var _chalk2 = _interopRequireDefault(_chalk);

var _clear = require('clear');

var _clear2 = _interopRequireDefault(_clear);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var styles = exports.styles = {
  separator: _chalk2.default.bold.grey
};

function ucFirst(str) {
  if (typeof str !== 'string' || str.length === 0) {
    return '';
  }
  return str.slice(0, 1).toUpperCase() + str.slice(1);
}

function separator() {
  var char = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : '-';
  var times = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 60;

  console.log(styles.separator(char.repeat(times)));
}

function info() {
  console.log.apply(null, [_chalk2.default.bgBlack.blue('    info ')].concat(Array.prototype.slice.call(arguments)));
}

function warn() {
  console.log.apply(null, [_chalk2.default.bgBlack.yellow('    warn ')].concat(Array.prototype.slice.call(arguments)));
}

function error() {
  console.log.apply(null, [_chalk2.default.bgBlack.red('   error ')].concat(Array.prototype.slice.call(arguments)));
}

function success() {
  console.log.apply(null, [_chalk2.default.bgBlack.green(' success ')].concat(Array.prototype.slice.call(arguments)));
}

function header(str) {
  var linesBefore = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 0;

  console.log(_chalk2.default.bgBlue.white('\n'.repeat(linesBefore) + ' >> ' + str + ' \n'));
}

function format(str, opts) {
  var output = str;
  var style = _chalk2.default;

  if (opts.bold) {
    style = style.bold;
  }

  if (opts.color) {
    style = style[opts.color];
  } else {
    style = style.white;
  }

  if (opts.ucFirst) {
    output = ucFirst(output);
  }

  return style(output);
}

function subHeader(str) {
  var opts = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};

  var linesBefore = opts.linesBefore | 0;
  var linesAfter = opts.linesAfter | 0;
  var format = opts.bold ? _chalk2.default.bold : _chalk2.default;
  console.log(format.yellow('\n'.repeat(linesBefore) + '--- ' + str + '\n'.repeat(linesAfter)));
}

function list(str) {
  console.log('  - ' + str);
}

function stack(str) {
  console.log(_chalk2.default.bold.grey('--- STACK ERROR ---'));
  console.log(_chalk2.default.grey(str));
}

exports.default = {
  format: format,
  ucFirst: ucFirst,
  styles: styles,
  clear: _clear2.default,
  separator: separator,
  header: header,
  subHeader: subHeader,
  info: info,
  success: success,
  warn: warn,
  error: error,
  stack: stack,
  list: list
};