#!/usr/bin/env node
'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.cli = exports.request = exports.config = undefined;

require('babel-polyfill');

var _parseArgs = require('./core/parseArgs');

var _parseArgs2 = _interopRequireDefault(_parseArgs);

var _print = require('./core/print');

var _print2 = _interopRequireDefault(_print);

var _config = require('./core/config');

var _config2 = _interopRequireDefault(_config);

var _handleError = require('./core/handleError');

var _handleError2 = _interopRequireDefault(_handleError);

var _plugins = require('./core/plugins');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * Display the footer and exit process
 * @param code
 */
function exit() {
  var code = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 0;

  _print2.default.separator();
  process.exit(code);
}

// Clear CLI
_print2.default.clear();

// Print header
_print2.default.separator('-');

/**
 * Parse arguments
 * @type {{args}|{plugin, command, args}}
 */
var args = (0, _parseArgs2.default)();

/**
 * Global configuration (.dot.json)
 * @type {Object}
 */
var config = exports.config = {};

/**
 * Current request
 * Contain all data related to the current request
 * @type {{args: (Array|*)}, plugin, command, config, data}
 */
var request = exports.request = { args: args.args

  /**
   * Application object pass across handlers
   * @type {{print: {format, ucFirst, styles, clear, separator, header, subHeader, info, success, warn, error, stack, list}, handleError: handleError, request: ({args: (Array|*)}|plugin|command|config|data), config: Object}}
   */
};var cli = exports.cli = {
  print: _print2.default,
  handleError: _handleError2.default,
  request: request,
  config: config

  // Load .dot.json config
};(0, _config2.default)().then(function (res) {
  Object.assign(config, res);

  // Get available and enabled plugins
  return (0, _plugins.getPlugins)(cli);
}).then(function (plugins) {
  // If plugin argument is not provided display command list for all plugins
  if (!args.plugin) {
    _print2.default.header('DOT -> Do Things for you !');
    var _iteratorNormalCompletion = true;
    var _didIteratorError = false;
    var _iteratorError = undefined;

    try {
      for (var _iterator = plugins[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
        var plugin = _step.value;

        (0, _plugins.printPluginCommands)(plugin);
      }
    } catch (err) {
      _didIteratorError = true;
      _iteratorError = err;
    } finally {
      try {
        if (!_iteratorNormalCompletion && _iterator.return) {
          _iterator.return();
        }
      } finally {
        if (_didIteratorError) {
          throw _iteratorError;
        }
      }
    }

    exit();
  }

  // Set requested plugin
  request.plugin = plugins.find(function (p) {
    return p.aliases.indexOf(args.plugin) !== -1;
  });

  // Check if requested plugin exist
  if (!request.plugin) {
    _print2.default.warn('Plugin ' + args.plugin + ' doesn\'t exist');
    exit();
  }

  // If command is not given, show all the plugins commands
  if (!args.command) {
    _print2.default.header('Plugin commands');
    (0, _plugins.printPluginCommands)(request.plugin);
    exit();
  }

  // Set the requested command
  request.command = request.plugin.commands.find(function (c) {
    return c.name === args.command;
  });

  // Check if requested command exist
  if (!request.command) {
    _print2.default.warn('[' + args.plugin + '] Command ' + args.command + ' doesn\'t exist');
    exit();
  }

  // Get plugin related configuration
  if (config.options[request.plugin.name]) {
    request.config = config.options[request.plugin.name];
  } else {
    // Or set it as an empty object
    request.config = {};
  }

  // If plugin has a before hook, execute it
  if (request.plugin.before) {
    request.plugin.before(cli);
  }

  // Finally, call the command handler
  (0, _plugins.handleRequest)(cli).then(function () {
    return exit();
  });
})
// Catch runtime errors
.catch(_handleError2.default);

exports.default = cli;