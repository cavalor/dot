'use strict';

var _path = require('path');

module.exports = function (cli) {
  var config = cli.request.config;
  var projectRoot = cli.config.projectRoot;


  if (config.path) {
    config.path = (0, _path.join)(projectRoot, config.path);
  } else {
    config.path = (0, _path.join)(projectRoot, 'database');
  }
};