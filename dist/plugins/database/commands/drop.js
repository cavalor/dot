'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = dropCommand;

var _db = require('../methods/db');

function dropCommand(cli) {
  var handleError = cli.handleError,
      print = cli.print;

  var dbPath = cli.request.config.path;

  return (0, _db.ensureDatabaseIsInitialized)(dbPath).then(function () {
    var _require = require(dbPath),
        sequelize = _require.sequelize;

    return sequelize.query('drop owned by ' + sequelize.config.username + ' cascade');
  }).then(function () {
    print.success('Database tables dropped');
  }).catch(handleError);
}