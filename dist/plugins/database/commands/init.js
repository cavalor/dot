'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

exports.default = function (cli) {
  var handleError = cli.handleError;
  var config = cli.request.config;


  return (0, _db.initializeDatabase)(config.path).catch(handleError);
};

var _db = require('../methods/db');