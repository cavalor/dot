'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = getFields;

var _inquirer = require('inquirer');

function getFields(cli) {
  var print = cli.print;
  var data = cli.request.data;


  if (!data) {
    throw Error('cli.request should provide data element as promise data bag');
  }

  print.subHeader('field');

  return (0, _inquirer.prompt)([{
    name: 'name',
    type: 'input',
    message: 'Enter the field name:',
    validate: function validate(val) {
      if (data.fields.find(function (f) {
        return f.name === val;
      })) {
        return 'Field ' + val + ' already declared';
      }
      return true;
    }
  }, {
    name: 'type',
    type: 'rawlist',
    choices: ['TEXT', 'JSON', 'JSONB', 'BOOLEAN', 'UUID', 'DATE', 'STRING', 'INTEGER', 'ENUM', 'JSONB', 'FLOAT'],
    message: 'Select field type',
    when: function when(res) {
      return res.name;
    }
  }, {
    name: 'typeArg',
    type: 'prompt',
    message: 'Is required field ?',
    when: function when(res) {
      return ['ENUM'].indexOf(res.type) > -1;
    }
  }, {
    name: 'required',
    type: 'confirm',
    message: 'Is required field ?',
    default: function _default() {
      return false;
    },
    when: function when(res) {
      return res.name;
    }
  }, {
    name: 'unique',
    type: 'confirm',
    message: 'Add unique constraint ?',
    default: function _default() {
      return false;
    },
    when: function when(res) {
      return res.name;
    }
  }]).then(function (res) {
    if (!res.name) {
      return Promise.resolve(cli);
    }

    var type = res.type;

    if (res.typeArg) {
      type += '(' + res.typeArg + ')';
    }

    data.fields.push({
      name: res.name,
      type: type,
      required: res.required,
      unique: res.unique
    });

    return getFields(cli);
  });
}