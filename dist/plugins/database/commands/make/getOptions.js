'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = getOptions;

var _inquirer = require('inquirer');

var _getPrimaryKey = require('./getPrimaryKey');

var _getPrimaryKey2 = _interopRequireDefault(_getPrimaryKey);

var _getFields = require('./getFields');

var _getFields2 = _interopRequireDefault(_getFields);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function getOptions(cli) {
  var print = cli.print;
  var data = cli.request.data;


  if (!data) {
    throw Error('cli.request should provide data element as promise data bag');
  }

  print.header('Entity creation tool');

  return (0, _inquirer.prompt)([{
    name: 'action',
    type: 'list',
    message: 'Select migration type',
    choices: ['create', 'alter', 'drop']
  }, {
    name: 'target',
    type: 'list',
    message: 'Select operation target',
    choices: ['entity', 'relation']
  }, {
    name: 'entity',
    type: 'input',
    message: 'Enter the entity name:',
    when: function when(res) {
      return res.target === 'entity';
    },
    validate: function validate(val) {
      return val.length ? true : 'Empty string is forbidden';
    }
  }, {
    name: 'firstEntity',
    type: 'input',
    message: 'Enter name of the first entity:',
    when: function when(res) {
      return res.target === 'relation';
    },
    validate: function validate(val) {
      return val.length ? true : 'Empty string is forbidden';
    }
  }, {
    name: 'secondEntity',
    type: 'input',
    message: 'Enter name of the second entity:',
    when: function when(res) {
      return res.target === 'relation';
    },
    validate: function validate(val) {
      return val.length ? true : 'Empty string is forbidden';
    }
  }]).then(function (res) {
    data.action = res.action;
    data.target = res.target;

    if (data.target === 'relation') {
      data.entities = [res.firstEntity, res.secondEntity];
    } else {
      data.entity = res.entity;
    }

    var timestamps = [{
      name: 'timestamps',
      type: 'confirm',
      message: 'Generate createdAt and updatedAt fields ?'
    }];
    var softDelete = [{
      name: 'softDelete',
      type: 'confirm',
      message: 'Enable soft delete ?',
      default: function _default() {
        return false;
      }
    }];
    var seed = [{
      name: 'generateSeed',
      type: 'confirm',
      default: false,
      message: 'Generate a seed file?'
    }, {
      name: 'seed',
      type: 'input',
      message: 'Set the seed file name',
      when: function when(res) {
        return res.generateSeed;
      },
      default: function _default() {
        return 'seed_' + data.entity;
      }
    }];

    if (data.action === 'create' && data.target === 'entity') {
      return (0, _inquirer.prompt)([].concat(timestamps, softDelete, seed)).then(function (res) {
        data.timestamps = res.timestamps;
        data.softDelete = res.softDelete;
        data.seed = res.seed || false;

        return Promise.resolve(cli);
      });
    }

    return data;
  }).then(function () {
    if (data.action === 'create' && data.target === 'entity') {
      return (0, _getPrimaryKey2.default)(cli).then(_getFields2.default);
    }

    return Promise.resolve(cli);
  });
}