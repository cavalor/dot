'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = getPrimaryKey;

var _inquirer = require('inquirer');

function getPrimaryKey(cli) {
  var print = cli.print;
  var data = cli.request.data;


  if (!data) {
    throw Error('cli.request should provide data element as promise data bag');
  }

  print.subHeader('primary key');

  return (0, _inquirer.prompt)([{
    name: 'id',
    type: 'confirm',
    message: 'Use "id" field as primary key ?',
    when: function when(answers) {
      return data.target === 'entity';
    }
  }, {
    name: 'name',
    type: 'prompt',
    message: 'Name of primary key field:',
    when: function when(answers) {
      return !answers.id;
    }
  }, {
    name: 'type',
    type: 'rawlist',
    choices: ['STRING', 'INTEGER', 'UUID'],
    message: 'Select field type',
    when: function when(answers) {
      return !answers.id;
    }
  }]).then(function (answers) {
    if (!answers.id && !answers.name.length) {
      return Promise.resolve(data);
    }

    var pk = {
      name: answers.id ? 'id' : answers.name,
      type: answers.id ? 'UUID' : answers.type,
      primaryKey: true
    };

    if (pk.type === 'UUID') {
      pk.default = 'DataTypes.UUIDV4';
    } else if (pk.type === 'INTEGER') {
      pk.autoIncrement = true;
    }

    data.fields.push(pk);

    return Promise.resolve(cli);
  });
}