'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = make;

var _createMigration = require('../../methods/createMigration');

var _createMigration2 = _interopRequireDefault(_createMigration);

var _createModel = require('../../methods/createModel');

var _createModel2 = _interopRequireDefault(_createModel);

var _createSeed = require('../../methods/createSeed');

var _createSeed2 = _interopRequireDefault(_createSeed);

var _db = require('../../methods/db');

var _getOptions = require('./getOptions');

var _getOptions2 = _interopRequireDefault(_getOptions);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function make(cli) {
  var handleError = cli.handleError;

  var dbPath = cli.request.config.path;

  cli.request.data = { fields: [] };

  return (0, _db.ensureDatabaseIsInitialized)(dbPath).then(function () {
    return (0, _getOptions2.default)(cli);
  }).then(function () {
    return console.log(cli.request.data);
  }).then(function () {
    return (0, _createMigration2.default)(cli);
  }).then(function () {
    return (0, _createModel2.default)(cli);
  }).then(function () {
    return (0, _createSeed2.default)(cli);
  }).catch(handleError);
}