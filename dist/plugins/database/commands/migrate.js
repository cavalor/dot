'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _slicedToArray = function () { function sliceIterator(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"]) _i["return"](); } finally { if (_d) throw _e; } } return _arr; } return function (arr, i) { if (Array.isArray(arr)) { return arr; } else if (Symbol.iterator in Object(arr)) { return sliceIterator(arr, i); } else { throw new TypeError("Invalid attempt to destructure non-iterable instance"); } }; }();

exports.default = migrate;

var _path = require('path');

var _path2 = _interopRequireDefault(_path);

var _db = require('../methods/db');

var _umzug = require('umzug');

var _umzug2 = _interopRequireDefault(_umzug);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } else { return Array.from(arr); } }

function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { step("next", value); }, function (err) { step("throw", err); }); } } return step("next"); }); }; }

function migrate(cli) {
  var _this = this;

  var print = cli.print,
      request = cli.request;

  var dbPath = request.config.path;

  var _request$args = _slicedToArray(request.args, 2),
      action = _request$args[0],
      parameter = _request$args[1];

  return (0, _db.ensureDatabaseIsInitialized)(dbPath).then(function () {
    var _require = require(request.config.path),
        sequelize = _require.sequelize;

    var umzug = new _umzug2.default({
      storage: 'sequelize',
      storageOptions: { sequelize: sequelize },
      // see: https://github.com/sequelize/umzug/issues/17
      migrations: {
        params: [sequelize.getQueryInterface(), // queryInterface
        sequelize.constructor // DataTypes
        ],
        path: _path2.default.join(dbPath, 'migrations'),
        pattern: /\.js$/
      },
      logging: null
    });

    umzug.on('migrating', function (name) {
      return print.info(name + ' migrating');
    });
    umzug.on('migrated', function (name) {
      return print.success(name + ' migrated');
    });
    umzug.on('reverting', function (name) {
      return print.info(name + ' reverting');
    });
    umzug.on('reverted', function (name) {
      return print.success(name + ' reverted');
    });

    // TODO: change code style
    // When SequelizeMeta table does'nt exist, concurrent calls generate an exception
    // const [executed, pending] = await Promise.all([
    //   umzug.executed(),
    //   umzug.pending()
    // ]) // fail
    var getStats = function () {
      var _ref = _asyncToGenerator(regeneratorRuntime.mark(function _callee() {
        var executed, pending, next, _arr, _i, m;

        return regeneratorRuntime.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                _context.next = 2;
                return umzug.executed();

              case 2:
                executed = _context.sent;
                _context.next = 5;
                return umzug.pending();

              case 5:
                pending = _context.sent;
                next = pending.length > 0 ? pending[0].name : '<NO_MIGRATIONS>';
                _arr = [].concat(_toConsumableArray(executed), _toConsumableArray(pending));


                for (_i = 0; _i < _arr.length; _i++) {
                  m = _arr[_i];

                  m.name = _path2.default.basename(m.file, '.js');
                }

                return _context.abrupt('return', { executed: executed, pending: pending, next: next });

              case 10:
              case 'end':
                return _context.stop();
            }
          }
        }, _callee, _this);
      }));

      return function getStats() {
        return _ref.apply(this, arguments);
      };
    }();

    var cmdStatus = function () {
      var _ref2 = _asyncToGenerator(regeneratorRuntime.mark(function _callee2() {
        var _ref3, executed, pending, _iteratorNormalCompletion, _didIteratorError, _iteratorError, _iterator, _step, migration, _iteratorNormalCompletion2, _didIteratorError2, _iteratorError2, _iterator2, _step2, _migration;

        return regeneratorRuntime.wrap(function _callee2$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                _context2.next = 2;
                return getStats();

              case 2:
                _ref3 = _context2.sent;
                executed = _ref3.executed;
                pending = _ref3.pending;

                if (!(executed.length > 0)) {
                  _context2.next = 26;
                  break;
                }

                print.subHeader('Executed migration (' + executed.length + ')', {
                  color: 'green',
                  linesBefore: 0
                });
                _iteratorNormalCompletion = true;
                _didIteratorError = false;
                _iteratorError = undefined;
                _context2.prev = 10;
                for (_iterator = executed[Symbol.iterator](); !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
                  migration = _step.value;

                  print.list(migration.name);
                }
                _context2.next = 18;
                break;

              case 14:
                _context2.prev = 14;
                _context2.t0 = _context2['catch'](10);
                _didIteratorError = true;
                _iteratorError = _context2.t0;

              case 18:
                _context2.prev = 18;
                _context2.prev = 19;

                if (!_iteratorNormalCompletion && _iterator.return) {
                  _iterator.return();
                }

              case 21:
                _context2.prev = 21;

                if (!_didIteratorError) {
                  _context2.next = 24;
                  break;
                }

                throw _iteratorError;

              case 24:
                return _context2.finish(21);

              case 25:
                return _context2.finish(18);

              case 26:
                if (!(pending.length > 0)) {
                  _context2.next = 47;
                  break;
                }

                print.subHeader('Pending migration (' + pending.length + ')');
                _iteratorNormalCompletion2 = true;
                _didIteratorError2 = false;
                _iteratorError2 = undefined;
                _context2.prev = 31;
                for (_iterator2 = pending[Symbol.iterator](); !(_iteratorNormalCompletion2 = (_step2 = _iterator2.next()).done); _iteratorNormalCompletion2 = true) {
                  _migration = _step2.value;

                  print.list(_migration.name);
                }
                _context2.next = 39;
                break;

              case 35:
                _context2.prev = 35;
                _context2.t1 = _context2['catch'](31);
                _didIteratorError2 = true;
                _iteratorError2 = _context2.t1;

              case 39:
                _context2.prev = 39;
                _context2.prev = 40;

                if (!_iteratorNormalCompletion2 && _iterator2.return) {
                  _iterator2.return();
                }

              case 42:
                _context2.prev = 42;

                if (!_didIteratorError2) {
                  _context2.next = 45;
                  break;
                }

                throw _iteratorError2;

              case 45:
                return _context2.finish(42);

              case 46:
                return _context2.finish(39);

              case 47:
              case 'end':
                return _context2.stop();
            }
          }
        }, _callee2, _this, [[10, 14, 18, 26], [19,, 21, 25], [31, 35, 39, 47], [40,, 42, 46]]);
      }));

      return function cmdStatus() {
        return _ref2.apply(this, arguments);
      };
    }();

    var cmdMigrate = function cmdMigrate(param) {
      return getStats().then(function (_ref4) {
        var pending = _ref4.pending;

        // For number type parameter, select the (n) next pending migrations
        if (typeof param === 'number') {
          return pending.slice(0, param).map(function (m) {
            return m.name;
          });
        }

        // For string type parameter, select migration where name match
        if (typeof param === 'string') {
          return pending.filter(function (m) {
            return [m.name, m.file].indexOf(param) > -1;
          }).map(function (m) {
            return m.name;
          });
        }

        // For null (or undefined) parameter, select all pending migrations
        return pending.map(function (m) {
          return m.name;
        });
      }).then(function () {
        var _ref5 = _asyncToGenerator(regeneratorRuntime.mark(function _callee3(migrations) {
          var migratedCount, _iteratorNormalCompletion3, _didIteratorError3, _iteratorError3, _iterator3, _step3, migration;

          return regeneratorRuntime.wrap(function _callee3$(_context3) {
            while (1) {
              switch (_context3.prev = _context3.next) {
                case 0:
                  migratedCount = 0;
                  _iteratorNormalCompletion3 = true;
                  _didIteratorError3 = false;
                  _iteratorError3 = undefined;
                  _context3.prev = 4;
                  _iterator3 = migrations[Symbol.iterator]();

                case 6:
                  if (_iteratorNormalCompletion3 = (_step3 = _iterator3.next()).done) {
                    _context3.next = 14;
                    break;
                  }

                  migration = _step3.value;
                  _context3.next = 10;
                  return umzug.up(migration);

                case 10:
                  migratedCount += 1;

                case 11:
                  _iteratorNormalCompletion3 = true;
                  _context3.next = 6;
                  break;

                case 14:
                  _context3.next = 20;
                  break;

                case 16:
                  _context3.prev = 16;
                  _context3.t0 = _context3['catch'](4);
                  _didIteratorError3 = true;
                  _iteratorError3 = _context3.t0;

                case 20:
                  _context3.prev = 20;
                  _context3.prev = 21;

                  if (!_iteratorNormalCompletion3 && _iterator3.return) {
                    _iterator3.return();
                  }

                case 23:
                  _context3.prev = 23;

                  if (!_didIteratorError3) {
                    _context3.next = 26;
                    break;
                  }

                  throw _iteratorError3;

                case 26:
                  return _context3.finish(23);

                case 27:
                  return _context3.finish(20);

                case 28:
                  // const migrated = await umzug.up(migrations)

                  if (migratedCount) {
                    print.subHeader('Result');
                    print.success(migratedCount + ' migrations successfully migrated');
                  } else {
                    print.warn('Nothing to migrate');
                  }

                case 29:
                case 'end':
                  return _context3.stop();
              }
            }
          }, _callee3, _this, [[4, 16, 20, 28], [21,, 23, 27]]);
        }));

        return function (_x) {
          return _ref5.apply(this, arguments);
        };
      }());
    };

    var cmdRevert = function cmdRevert(param) {
      return getStats().then(function (_ref6) {
        var executed = _ref6.executed;

        // For number type parameter, select the (n) last migrated
        if (typeof param === 'number') {
          return executed.slice(param * -1).map(function (m) {
            return m.name;
          });
        }

        // For string type parameter, select migration where name match
        if (typeof param === 'string') {
          return executed.filter(function (m) {
            return [m.name, m.file].indexOf(param) > -1;
          }).map(function (m) {
            return m.name;
          });
        }

        // For null parameter (or undefined) revert all
        return [];
      }).then(function () {
        var _ref7 = _asyncToGenerator(regeneratorRuntime.mark(function _callee4(migrations) {
          var migrated;
          return regeneratorRuntime.wrap(function _callee4$(_context4) {
            while (1) {
              switch (_context4.prev = _context4.next) {
                case 0:
                  _context4.next = 2;
                  return umzug.down(migrations);

                case 2:
                  migrated = _context4.sent;

                  if (migrated.length) {
                    print.subHeader('Result');
                    print.success(migrated.length + ' migrations successfully reverted');
                  } else {
                    print.warn('Nothing to revert');
                  }

                case 4:
                case 'end':
                  return _context4.stop();
              }
            }
          }, _callee4, _this);
        }));

        return function (_x2) {
          return _ref7.apply(this, arguments);
        };
      }());
    };

    function getCmd(name) {
      switch (name) {
        case 'status':
          return cmdStatus;
        case 'up':
        case 'migrate':
          return cmdMigrate;
        case 'down':
        case 'revert':
          return cmdRevert;
        default:
          parameter = action;
          return cmdMigrate;
      }
    }

    var cmd = getCmd(action);

    return cmd(parameter).catch(function (err) {
      print.error(err);
    });
  });
}