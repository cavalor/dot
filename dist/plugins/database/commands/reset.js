'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

exports.default = function (cli) {
  var print = cli.print;

  print.subHeader('Drop database tables');
  return (0, _drop2.default)(cli).then(function () {
    print.subHeader('Start migrating database', { linesBefore: 1 });
    return (0, _migrate2.default)(cli);
  }).then(function () {
    print.subHeader('Start seeding database', { linesBefore: 1 });
    return (0, _seed2.default)(cli);
  });
};

var _drop = require('./drop');

var _drop2 = _interopRequireDefault(_drop);

var _migrate = require('./migrate');

var _migrate2 = _interopRequireDefault(_migrate);

var _seed = require('./seed');

var _seed2 = _interopRequireDefault(_seed);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }