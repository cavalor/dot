'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

exports.default = function (cli) {
  var _this = this;

  var print = cli.print,
      handleError = cli.handleError;

  var dbPath = cli.request.config.path;
  var db = require(dbPath);
  var seedsPath = _path2.default.join(dbPath, 'seeds');

  function getSeedFileNames() {
    return _fs2.default.readdirSync(seedsPath);
  }

  return (0, _db.ensureDatabaseIsInitialized)(dbPath).then(getSeedFileNames).then(function () {
    var _ref = _asyncToGenerator(regeneratorRuntime.mark(function _callee(files) {
      var _iteratorNormalCompletion, _didIteratorError, _iteratorError, _iterator, _step, file, seed;

      return regeneratorRuntime.wrap(function _callee$(_context) {
        while (1) {
          switch (_context.prev = _context.next) {
            case 0:
              if (files.length) {
                _context.next = 4;
                break;
              }

              return _context.abrupt('return', print.info('Nothing to process'));

            case 4:
              _iteratorNormalCompletion = true;
              _didIteratorError = false;
              _iteratorError = undefined;
              _context.prev = 7;
              _iterator = files[Symbol.iterator]();

            case 9:
              if (_iteratorNormalCompletion = (_step = _iterator.next()).done) {
                _context.next = 23;
                break;
              }

              file = _step.value;
              _context.prev = 11;
              seed = require(_path2.default.join(seedsPath, file));
              _context.next = 15;
              return seed(db, _faker2.default);

            case 15:
              _context.next = 20;
              break;

            case 17:
              _context.prev = 17;
              _context.t0 = _context['catch'](11);

              handleError(_context.t0);

            case 20:
              _iteratorNormalCompletion = true;
              _context.next = 9;
              break;

            case 23:
              _context.next = 29;
              break;

            case 25:
              _context.prev = 25;
              _context.t1 = _context['catch'](7);
              _didIteratorError = true;
              _iteratorError = _context.t1;

            case 29:
              _context.prev = 29;
              _context.prev = 30;

              if (!_iteratorNormalCompletion && _iterator.return) {
                _iterator.return();
              }

            case 32:
              _context.prev = 32;

              if (!_didIteratorError) {
                _context.next = 35;
                break;
              }

              throw _iteratorError;

            case 35:
              return _context.finish(32);

            case 36:
              return _context.finish(29);

            case 37:
              print.subHeader('Result');
              print.success('Database seeded successfully');

            case 39:
            case 'end':
              return _context.stop();
          }
        }
      }, _callee, _this, [[7, 25, 29, 37], [11, 17], [30,, 32, 36]]);
    }));

    return function (_x) {
      return _ref.apply(this, arguments);
    };
  }()).catch(handleError);
};

var _fs = require('fs');

var _fs2 = _interopRequireDefault(_fs);

var _path = require('path');

var _path2 = _interopRequireDefault(_path);

var _faker = require('faker');

var _faker2 = _interopRequireDefault(_faker);

var _db = require('../methods/db');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { step("next", value); }, function (err) { step("throw", err); }); } } return step("next"); }); }; }