'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = {
  aliases: ['db', 'database'],

  description: 'Database management plugin',

  commands: [{
    name: 'init',
    description: 'Initialize database'
  }, {
    name: 'drop',
    description: 'Drop all database tables'
  }, {
    name: 'make',
    description: 'Create migration, model and seed'
  }, {
    name: 'migrate',
    description: 'Execute migrations'
  }, {
    name: 'seed',
    description: 'Execute database seeds'
  }, {
    name: 'reset',
    description: 'Drop tables, launch migration and seed database'
  }],

  before: function before(cli) {
    require('./before/patchConfig')(cli);
  }
};