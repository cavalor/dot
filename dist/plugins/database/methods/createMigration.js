'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = createMigration;

var _fs = require('fs');

var _fs2 = _interopRequireDefault(_fs);

var _path = require('path');

var _path2 = _interopRequireDefault(_path);

var _pluralize = require('pluralize');

var _nunjucks = require('nunjucks');

var _lodash = require('lodash');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } else { return Array.from(arr); } }

var templatePath = _path2.default.join(__dirname, '../templates/migration.nunj');

var entityName = function entityName(str) {
  return (0, _lodash.upperFirst)((0, _lodash.camelCase)((0, _pluralize.singular)(str)));
};

function createMigration(cli) {
  var print = cli.print;
  var data = cli.request.data;

  var dbPath = cli.request.config.path;

  var table = '';
  var fields = [];

  if (data.target === 'relation') {
    var entities = [].concat(_toConsumableArray(data.entities)).map(function (e) {
      return entityName(e);
    }).sort(function (a, b) {
      return a > b;
    });

    fields = entities.map(function (e) {
      return {
        name: (0, _lodash.lowerFirst)(e) + 'Id',
        type: 'UUID',
        references: {
          model: e,
          key: 'id'
        },
        onUpdate: 'cascade',
        primaryKey: true
      };
    });

    var _iteratorNormalCompletion = true;
    var _didIteratorError = false;
    var _iteratorError = undefined;

    try {
      for (var _iterator = entities[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
        var e = _step.value;

        table = table.length ? table + '_' + e : e;
      }
    } catch (err) {
      _didIteratorError = true;
      _iteratorError = err;
    } finally {
      try {
        if (!_iteratorNormalCompletion && _iterator.return) {
          _iterator.return();
        }
      } finally {
        if (_didIteratorError) {
          throw _iteratorError;
        }
      }
    }
  } else if (data.target === 'entity') {
    table = entityName(data.entity);
    fields = data.fields;
  } else {
    return Promise.reject(new Error('Target type not supported: ' + data.target));
  }

  if (data.timestamps) {
    fields.push({ name: 'createdAt', type: 'DATE' }, { name: 'updatedAt', type: 'DATE' });
  }

  if (data.softDelete) {
    fields.push({ name: 'deletedAt', type: 'DATE' });
  }

  var fileName = Date.now() + '_' + data.action + '_' + table + '.js';

  return new Promise(function (resolve, reject) {
    try {
      // Nunjucks render doesn't work with config absolute path
      var template = _fs2.default.readFileSync(templatePath);
      var templateString = template.toString();

      (0, _nunjucks.renderString)(templateString, {
        action: data.action,
        table: table,
        fields: fields
      }, function (err, compiledTemplate) {
        if (err) return reject(err);

        var targetPath = _path2.default.join(dbPath, 'migrations', fileName);

        _fs2.default.writeFile(targetPath, compiledTemplate, function (err) {
          if (err) return reject(err);

          print.success('Migration created: ' + targetPath);

          resolve(targetPath);
        });
      });
    } catch (err) {
      reject(err);
    }
  });
}