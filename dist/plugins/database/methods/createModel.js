'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = createModel;
var fs = require('fs');
var path = require('path');

var _require = require('nunjucks'),
    renderString = _require.renderString;

var _require2 = require('lodash'),
    upperFirst = _require2.upperFirst,
    camelCase = _require2.camelCase;

var _require3 = require('pluralize'),
    singular = _require3.singular;

var templatePath = path.join(__dirname, '../templates/model.nunj');

function createModel(cli) {
  var print = cli.print;
  var data = cli.request.data;

  var dbPath = cli.request.config.path;

  return new Promise(function (resolve, reject) {
    if (!data.target || data.target !== 'entity') return resolve();

    var modelName = upperFirst(camelCase(singular(data.entity)));
    var filePath = path.join(dbPath, 'models', modelName + '.js');
    var fileExist = fs.existsSync(filePath);

    if (fileExist) {
      print.info('[skipped] File already exist: ' + filePath);
      resolve();
    } else {
      try {
        // Nunjucks render doesn't work with config absolute path
        var file = fs.readFileSync(templatePath);
        var templateString = file.toString();

        renderString(templateString, {
          model: modelName,
          timestamps: data.timestamps,
          softDelete: data.softDelete,
          fields: data.fields
        }, function (err, res) {
          if (err) return reject(err);

          fs.writeFile(filePath, res, function (err) {
            if (err) return reject(err);

            print.success('Model created: ' + filePath);

            resolve(filePath);
          });
        });
      } catch (err) {
        reject(err);
      }
    }
  });
}