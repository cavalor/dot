'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = createSeed;

var _lodash = require('lodash');

var _nunjucks = require('nunjucks');

var _fs = require('fs');

var _fs2 = _interopRequireDefault(_fs);

var _path = require('path');

var _path2 = _interopRequireDefault(_path);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var templatePath = _path2.default.join(__dirname, '../templates/seed.nunj');

function createSeed(cli) {
  var print = cli.print;
  var data = cli.request.data;

  var dbPath = cli.request.config.path;

  return new Promise(function (resolve, reject) {
    if (!data.seed) return resolve();

    var fileName = Date.now() + '_' + (0, _lodash.snakeCase)(data.seed) + '.js';
    var filePath = _path2.default.join(dbPath, 'seeds', fileName);
    var fileExist = _fs2.default.existsSync(filePath);

    if (fileExist) {
      print.info('[skipped] File already exist: ' + filePath);
      return resolve(filePath);
    }

    try {
      // Nunjucks render doesn't work with config absolute path
      var template = _fs2.default.readFileSync(templatePath).toString();

      (0, _nunjucks.renderString)(template, {
        header: (0, _lodash.upperFirst)(data.seed),
        model: (0, _lodash.upperFirst)((0, _lodash.camelCase)(data.entity))
      }, function (err, res) {
        if (err) return reject(err);

        _fs2.default.writeFile(filePath, res, function (err) {
          if (err) return reject(err);

          print.success('Seed created: ' + filePath);

          resolve(filePath);
        });
      });
    } catch (err) {
      reject(err);
    }
  });
}