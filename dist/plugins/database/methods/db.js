'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.ensureDatabaseIsInitialized = exports.initializeDatabase = undefined;

var initializeDatabase = exports.initializeDatabase = function () {
  var _ref = _asyncToGenerator(regeneratorRuntime.mark(function _callee(dbPath) {
    var isInit;
    return regeneratorRuntime.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            if (dbPath) {
              _context.next = 2;
              break;
            }

            throw Error('Database path argument is required');

          case 2:
            _context.next = 4;
            return databaseIsInitialized(dbPath);

          case 4:
            isInit = _context.sent;

            if (!isInit) {
              _context.next = 8;
              break;
            }

            _print2.default.warn('Database already initialized');

            return _context.abrupt('return', Promise.resolve());

          case 8:
            _context.next = 10;
            return copyTemplateToDestPath(dbPath);

          case 10:

            _print2.default.success('Database initialized');

          case 11:
          case 'end':
            return _context.stop();
        }
      }
    }, _callee, this);
  }));

  return function initializeDatabase(_x) {
    return _ref.apply(this, arguments);
  };
}();

var ensureDatabaseIsInitialized = exports.ensureDatabaseIsInitialized = function () {
  var _ref2 = _asyncToGenerator(regeneratorRuntime.mark(function _callee2(dbPath) {
    var isInit;
    return regeneratorRuntime.wrap(function _callee2$(_context2) {
      while (1) {
        switch (_context2.prev = _context2.next) {
          case 0:
            if (dbPath) {
              _context2.next = 2;
              break;
            }

            throw Error('Database path argument is required');

          case 2:
            _context2.next = 4;
            return databaseIsInitialized(dbPath);

          case 4:
            isInit = _context2.sent;

            if (isInit) {
              _context2.next = 8;
              break;
            }

            _context2.next = 8;
            return initializeDatabase(dbPath);

          case 8:
          case 'end':
            return _context2.stop();
        }
      }
    }, _callee2, this);
  }));

  return function ensureDatabaseIsInitialized(_x2) {
    return _ref2.apply(this, arguments);
  };
}();

exports.databaseIsInitialized = databaseIsInitialized;
exports.copyTemplateToDestPath = copyTemplateToDestPath;

var _fs = require('fs');

var _fs2 = _interopRequireDefault(_fs);

var _path = require('path');

var _path2 = _interopRequireDefault(_path);

var _ncp = require('ncp');

var _print = require('../../../core/print');

var _print2 = _interopRequireDefault(_print);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { step("next", value); }, function (err) { step("throw", err); }); } } return step("next"); }); }; }

function databaseIsInitialized(dbPath) {
  return new Promise(function (resolve) {
    _fs2.default.stat(dbPath, function (err) {
      resolve(!err);
    });
  });
}

function copyTemplateToDestPath(destPath) {
  var templatePath = _path2.default.join(__dirname, '../templates/db');

  return new Promise(function (resolve) {
    (0, _ncp.ncp)(templatePath, destPath, function (err) {
      if (err) {
        throw err;
      }

      _fs2.default.mkdirSync(_path2.default.join(destPath, 'migrations'));
      _fs2.default.mkdirSync(_path2.default.join(destPath, 'models'));
      _fs2.default.mkdirSync(_path2.default.join(destPath, 'seeds'));

      resolve();
    });
  });
}