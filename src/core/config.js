import fs from 'fs'
import path from 'path'

function getConfigPath () {
  let basePath = process.cwd()
  let testsCount = 0
  let shouldContinue = true

  while (shouldContinue) {
    try {
      if (testsCount > 0) {
        basePath = path.join(basePath, '../')
      }

      const testPath = path.join(basePath, '.dot.json')

      fs.statSync(testPath)

      return testPath
    } catch (err) {
      if (testsCount < 5) {
        testsCount++
      } else {
        shouldContinue = false
        throw new Error('Please create a ".dot.json" file on project root')
      }
    }
  }
}

export default function getConfig () {
  return new Promise((resolve, reject) => {
    try {
      const configPath = getConfigPath()
      const config = require(configPath)

      if (!config.options) {
        config.options = {}
      }

      resolve({ projectRoot: path.dirname(configPath), ...config })
    } catch (err) {
      reject(err)
    }
  })
}
