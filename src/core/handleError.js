import print from './print'

export default function handleError (err) {
  if (err.message) {
    print.error(err.message)
    print.stack(err.stack)
  } else {
    print.error(err)
  }
  print.separator('-')
  process.exit()
}
