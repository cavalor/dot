import { argv } from 'yargs'

export default function parseArgs () {
  const args = argv['_']

  if (args.length === 0) {
    return {args: []}
  }

  if (args[0].indexOf(':') === -1) {
    const [plugin, command] = args

    return {plugin, command, args: args.slice(2)}
  }

  const [plugin, command] = args[0].split(':')

  return {plugin, command, args: args.slice(1)}
}
