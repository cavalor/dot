import fs from 'fs'
import path from 'path'
import print from './print'

const pluginDir = path.join(__dirname, '../plugins')

/**
 * Import plugin definition file
 * @param pluginName
 */
export function getPlugin (pluginName) {
  const plugin = require(path.join(pluginDir, pluginName)).default

  plugin.name = pluginName

  return plugin
}

/**
 * Get all available plugins then filter them with the list given in .dot.json
 * @param cli
 * @returns {Promise}
 */
export function getPlugins (cli) {
  const { config } = cli

  return new Promise((resolve, reject) => {
    if (!config) {
      return reject(Error('"Config" should be provided by cli interface'))
    }

    if (!config.plugins) {
      return reject(Error('You should provide a "plugins" entry in .dot.json config'))
    }

    // Read plugin directory
    fs.readdir(pluginDir, (err, availablePlugins) => {
      if (err) {
        return reject(err)
      }

      const enabledPlugins = availablePlugins
        .filter(f => config.plugins.indexOf(f) !== -1)

      resolve(enabledPlugins.map(getPlugin))
    })
  })
}

/**
 * List all commands for a given plugin
 * @param plugin {String}
 */
export function printPluginCommands (plugin) {
  const {commands, name} = plugin
  const namesLength = commands.map(c => c.name.length)
  const maxLength = Math.max(...namesLength)

  let output = print.format(name, { ucFirst: true, bold: true })

  for (const command of commands) {
    output = output.concat('\n', print.format(' * '.concat(command.name), { bold: true }))
    output = output.concat(' '.repeat(maxLength - command.name.length))
    output = output.concat(print.format(' | '.concat(command.description), { color: 'grey' }))
  }

  console.log(output, '\n')
}

/**
 * Require handler related to the current request information
 * @param cli {Object}
 */
export function handleRequest (cli) {
  const { plugin, command } = cli.request

  print.header(`${print.ucFirst(plugin.name)}::${command.name}`)

  const cmd = require(path.join(pluginDir, plugin.name, 'commands', command.name)).default
  return cmd(cli)
}
