import chalk from 'chalk'
import clear from 'clear'

export const styles = {
  separator: chalk.bold.grey
}

function ucFirst (str) {
  if (typeof str !== 'string' || str.length === 0) {
    return ''
  }
  return str.slice(0, 1).toUpperCase() + str.slice(1)
}

function separator (char = '-', times = 60) {
  console.log(styles.separator(char.repeat(times)))
}

function info () {
  console.log.apply(null, [chalk.bgBlack.blue('    info '), ...arguments])
}

function warn () {
  console.log.apply(null, [chalk.bgBlack.yellow('    warn '), ...arguments])
}

function error () {
  console.log.apply(null, [chalk.bgBlack.red('   error '), ...arguments])
}

function success () {
  console.log.apply(null, [chalk.bgBlack.green(' success '), ...arguments])
}

function header (str, linesBefore = 0) {
  console.log(chalk.bgBlue.white(`${'\n'.repeat(linesBefore)} >> ${str} \n`))
}

function format (str, opts) {
  let output = str
  let style = chalk

  if (opts.bold) {
    style = style.bold
  }

  if (opts.color) {
    style = style[opts.color]
  } else {
    style = style.white
  }

  if (opts.ucFirst) {
    output = ucFirst(output)
  }

  return style(output)
}

function subHeader (str, opts = {}) {
  const linesBefore = opts.linesBefore | 0
  const linesAfter = opts.linesAfter | 0
  const format = opts.bold ? chalk.bold : chalk
  console.log(format.yellow(`${'\n'.repeat(linesBefore)}--- ${str}${'\n'.repeat(linesAfter)}`))
}

function list (str) {
  console.log(`  - ${str}`)
}

function stack (str) {
  console.log(chalk.bold.grey('--- STACK ERROR ---'))
  console.log(chalk.grey(str))
}

export default {
  format,
  ucFirst,
  styles,
  clear,
  separator,
  header,
  subHeader,
  info,
  success,
  warn,
  error,
  stack,
  list
}
