#!/usr/bin/env node

import 'babel-polyfill'
import parseArgs from './core/parseArgs'
import print from './core/print'
import getConfig from './core/config'
import handleError from './core/handleError'
import { getPlugins, printPluginCommands, handleRequest } from './core/plugins'

/**
 * Display the footer and exit process
 * @param code
 */
function exit (code = 0) {
  print.separator()
  process.exit(code)
}

// Clear CLI
print.clear()

// Print header
print.separator('-')

/**
 * Parse arguments
 * @type {{args}|{plugin, command, args}}
 */
const args = parseArgs()

/**
 * Global configuration (.dot.json)
 * @type {Object}
 */
export const config = {}

/**
 * Current request
 * Contain all data related to the current request
 * @type {{args: (Array|*)}, plugin, command, config, data}
 */
export const request = { args: args.args }

/**
 * Application object pass across handlers
 * @type {{print: {format, ucFirst, styles, clear, separator, header, subHeader, info, success, warn, error, stack, list}, handleError: handleError, request: ({args: (Array|*)}|plugin|command|config|data), config: Object}}
 */
export const cli = {
  print,
  handleError,
  request,
  config
}

// Load .dot.json config
getConfig().then((res) => {
  Object.assign(config, res)

  // Get available and enabled plugins
  return getPlugins(cli)
}).then(plugins => {
  // If plugin argument is not provided display command list for all plugins
  if (!args.plugin) {
    print.header('DOT -> Do Things for you !')
    for (const plugin of plugins) {
      printPluginCommands(plugin)
    }
    exit()
  }

  // Set requested plugin
  request.plugin = plugins.find(p => (p.aliases.indexOf(args.plugin) !== -1))

  // Check if requested plugin exist
  if (!request.plugin) {
    print.warn(`Plugin ${args.plugin} doesn't exist`)
    exit()
  }

  // If command is not given, show all the plugins commands
  if (!args.command) {
    print.header('Plugin commands')
    printPluginCommands(request.plugin)
    exit()
  }

  // Set the requested command
  request.command = request.plugin.commands
    .find(c => c.name === args.command)

  // Check if requested command exist
  if (!request.command) {
    print.warn(`[${args.plugin}] Command ${args.command} doesn't exist`)
    exit()
  }

  // Get plugin related configuration
  if (config.options[request.plugin.name]) {
    request.config = config.options[request.plugin.name]
  } else {
    // Or set it as an empty object
    request.config = {}
  }

  // If plugin has a before hook, execute it
  if (request.plugin.before) {
    request.plugin.before(cli)
  }

  // Finally, call the command handler
  handleRequest(cli)
    .then(() => exit())
})
  // Catch runtime errors
  .catch(handleError)

export default cli
