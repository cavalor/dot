import { join } from 'path'

module.exports = function (cli) {
  const { config } = cli.request
  const { projectRoot } = cli.config

  if (config.path) {
    config.path = join(projectRoot, config.path)
  } else {
    config.path = join(projectRoot, 'database')
  }
}
