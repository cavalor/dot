import { ensureDatabaseIsInitialized } from '../methods/db'

export default function dropCommand (cli) {
  const { handleError, print } = cli
  const dbPath = cli.request.config.path

  return ensureDatabaseIsInitialized(dbPath).then(() => {
    const { sequelize } = require(dbPath)

    return sequelize.query(`drop owned by ${sequelize.config.username} cascade`)
  }).then(() => {
    print.success('Database tables dropped')
  }).catch(handleError)
}
