import { initializeDatabase } from '../methods/db'

export default function (cli) {
  const { handleError } = cli
  const { config } = cli.request

  return initializeDatabase(config.path)
    .catch(handleError)
}
