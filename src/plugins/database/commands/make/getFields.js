import { prompt } from 'inquirer'

export default function getFields (cli) {
  const { print } = cli
  const { data } = cli.request

  if (!data) {
    throw Error('cli.request should provide data element as promise data bag')
  }

  print.subHeader('field')

  return prompt([
    {
      name: 'name',
      type: 'input',
      message: 'Enter the field name:',
      validate (val) {
        if (data.fields.find(f => f.name === val)) {
          return `Field ${val} already declared`
        }
        return true
      }
    },
    {
      name: 'type',
      type: 'rawlist',
      choices: ['TEXT', 'JSON', 'JSONB', 'BOOLEAN', 'UUID', 'DATE', 'STRING', 'INTEGER', 'ENUM', 'JSONB', 'FLOAT'],
      message: 'Select field type',
      when: res => res.name
    },
    {
      name: 'typeArg',
      type: 'prompt',
      message: 'Is required field ?',
      when: res => ['ENUM'].indexOf(res.type) > -1
    },
    {
      name: 'required',
      type: 'confirm',
      message: 'Is required field ?',
      default: () => false,
      when: res => res.name
    },
    {
      name: 'unique',
      type: 'confirm',
      message: 'Add unique constraint ?',
      default: () => false,
      when: res => res.name
    }
  ]).then(res => {
    if (!res.name) {
      return Promise.resolve(cli)
    }

    let type = res.type

    if (res.typeArg) {
      type += `(${res.typeArg})`
    }

    data.fields.push({
      name: res.name,
      type,
      required: res.required,
      unique: res.unique
    })

    return getFields(cli)
  })
}
