import { prompt } from 'inquirer'
import getPrimaryKey from './getPrimaryKey'
import getFields from './getFields'

export default function getOptions (cli) {
  const { print } = cli
  const { data } = cli.request

  if (!data) {
    throw Error('cli.request should provide data element as promise data bag')
  }

  print.header('Entity creation tool')

  return prompt([
    {
      name: 'action',
      type: 'list',
      message: 'Select migration type',
      choices: ['create', 'alter', 'drop']
    },
    {
      name: 'target',
      type: 'list',
      message: 'Select operation target',
      choices: ['entity', 'relation']
    },
    {
      name: 'entity',
      type: 'input',
      message: 'Enter the entity name:',
      when: res => (res.target === 'entity'),
      validate (val) {
        return (val.length) ? true : 'Empty string is forbidden'
      }
    },
    {
      name: 'firstEntity',
      type: 'input',
      message: 'Enter name of the first entity:',
      when: res => (res.target === 'relation'),
      validate (val) {
        return (val.length) ? true : 'Empty string is forbidden'
      }
    },
    {
      name: 'secondEntity',
      type: 'input',
      message: 'Enter name of the second entity:',
      when: res => (res.target === 'relation'),
      validate (val) {
        return (val.length) ? true : 'Empty string is forbidden'
      }
    }
  ]).then(res => {
    data.action = res.action
    data.target = res.target

    if (data.target === 'relation') {
      data.entities = [res.firstEntity, res.secondEntity]
    } else {
      data.entity = res.entity
    }

    const timestamps = [
      {
        name: 'timestamps',
        type: 'confirm',
        message: 'Generate createdAt and updatedAt fields ?'
      }
    ]
    const softDelete = [
      {
        name: 'softDelete',
        type: 'confirm',
        message: 'Enable soft delete ?',
        default: () => false
      }
    ]
    const seed = [
      {
        name: 'generateSeed',
        type: 'confirm',
        default: false,
        message: 'Generate a seed file?'
      },
      {
        name: 'seed',
        type: 'input',
        message: 'Set the seed file name',
        when: res => res.generateSeed,
        default: () => `seed_${data.entity}`
      }
    ]

    if (data.action === 'create' && data.target === 'entity') {
      return prompt([
        ...timestamps,
        ...softDelete,
        ...seed
      ]).then(res => {
        data.timestamps = res.timestamps
        data.softDelete = res.softDelete
        data.seed = res.seed || false

        return Promise.resolve(cli)
      })
    }

    return data
  }).then(() => {
    if (data.action === 'create' && data.target === 'entity') {
      return getPrimaryKey(cli).then(getFields)
    }

    return Promise.resolve(cli)
  })
}
