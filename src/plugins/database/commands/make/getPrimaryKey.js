import { prompt } from 'inquirer'

export default function getPrimaryKey (cli) {
  const { print } = cli
  const { data } = cli.request

  if (!data) {
    throw Error('cli.request should provide data element as promise data bag')
  }

  print.subHeader('primary key')

  return prompt([
    {
      name: 'id',
      type: 'confirm',
      message: 'Use "id" field as primary key ?',
      when: answers => (data.target === 'entity')
    },
    {
      name: 'name',
      type: 'prompt',
      message: 'Name of primary key field:',
      when: answers => !answers.id
    },
    {
      name: 'type',
      type: 'rawlist',
      choices: ['STRING', 'INTEGER', 'UUID'],
      message: 'Select field type',
      when: answers => !answers.id
    }
  ]).then(answers => {
    if (!answers.id && !answers.name.length) {
      return Promise.resolve(data)
    }

    const pk = {
      name: answers.id ? 'id' : answers.name,
      type: answers.id ? 'UUID' : answers.type,
      primaryKey: true
    }

    if (pk.type === 'UUID') {
      pk.default = 'DataTypes.UUIDV4'
    } else if (pk.type === 'INTEGER') {
      pk.autoIncrement = true
    }

    data.fields.push(pk)

    return Promise.resolve(cli)
  })
}
