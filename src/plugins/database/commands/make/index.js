import createMigration from '../../methods/createMigration'
import createModel from '../../methods/createModel'
import createSeed from '../../methods/createSeed'
import { ensureDatabaseIsInitialized } from '../../methods/db'
import getOptions from './getOptions'

export default function make (cli) {
  const { handleError } = cli
  const dbPath = cli.request.config.path

  cli.request.data = { fields: [] }

  return ensureDatabaseIsInitialized(dbPath)
    .then(() => getOptions(cli))
    .then(() => console.log(cli.request.data))
    .then(() => createMigration(cli))
    .then(() => createModel(cli))
    .then(() => createSeed(cli))
    .catch(handleError)
}
