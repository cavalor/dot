import path from 'path'
import { ensureDatabaseIsInitialized } from '../methods/db'
import Umzug from 'umzug'

export default function migrate (cli) {
  const { print, request } = cli
  const dbPath = request.config.path
  let [action, parameter] = request.args

  return ensureDatabaseIsInitialized(dbPath).then(() => {
    const { sequelize } = require(request.config.path)

    const umzug = new Umzug({
      storage: 'sequelize',
      storageOptions: {sequelize},
      // see: https://github.com/sequelize/umzug/issues/17
      migrations: {
        params: [
          sequelize.getQueryInterface(), // queryInterface
          sequelize.constructor // DataTypes
        ],
        path: path.join(dbPath, 'migrations'),
        pattern: /\.js$/
      },
      logging: null
    })

    umzug.on('migrating', name => print.info(`${name} migrating`))
    umzug.on('migrated', name => print.success(`${name} migrated`))
    umzug.on('reverting', name => print.info(`${name} reverting`))
    umzug.on('reverted', name => print.success(`${name} reverted`))

    // TODO: change code style
    // When SequelizeMeta table does'nt exist, concurrent calls generate an exception
    // const [executed, pending] = await Promise.all([
    //   umzug.executed(),
    //   umzug.pending()
    // ]) // fail
    const getStats = async () => {
      const executed = await umzug.executed()
      const pending = await umzug.pending()
      const next = pending.length > 0 ? pending[0].name : '<NO_MIGRATIONS>'

      for (const m of [...executed, ...pending]) {
        m.name = path.basename(m.file, '.js')
      }

      return {executed, pending, next}
    }

    const cmdStatus = async () => {
      const {executed, pending} = await getStats()

      if (executed.length > 0) {
        print.subHeader(`Executed migration (${executed.length})`, {
          color: 'green',
          linesBefore: 0
        })
        for (const migration of executed) {
          print.list(migration.name)
        }
      }

      if (pending.length > 0) {
        print.subHeader(`Pending migration (${pending.length})`)
        for (const migration of pending) {
          print.list(migration.name)
        }
      }
    }

    const cmdMigrate = param => {
      return getStats().then(({pending}) => {
        // For number type parameter, select the (n) next pending migrations
        if (typeof param === 'number') {
          return pending
            .slice(0, param)
            .map(m => m.name)
        }

        // For string type parameter, select migration where name match
        if (typeof param === 'string') {
          return pending
            .filter(m => [m.name, m.file].indexOf(param) > -1)
            .map(m => m.name)
        }

        // For null (or undefined) parameter, select all pending migrations
        return pending.map(m => m.name)
      }).then(async migrations => {
        let migratedCount = 0

        for (const migration of migrations) {
          await umzug.up(migration)
          migratedCount += 1
        }
        // const migrated = await umzug.up(migrations)

        if (migratedCount) {
          print.subHeader('Result')
          print.success(`${migratedCount} migrations successfully migrated`)
        } else {
          print.warn('Nothing to migrate')
        }
      })
    }

    const cmdRevert = (param) => {
      return getStats().then(({executed}) => {
        // For number type parameter, select the (n) last migrated
        if (typeof param === 'number') {
          return executed
            .slice(param * -1)
            .map(m => m.name)
        }

        // For string type parameter, select migration where name match
        if (typeof param === 'string') {
          return executed
            .filter(m => [m.name, m.file].indexOf(param) > -1)
            .map(m => m.name)
        }

        // For null parameter (or undefined) revert all
        return []
      }).then(async migrations => {
        const migrated = await umzug.down(migrations)
        if (migrated.length) {
          print.subHeader('Result')
          print.success(`${migrated.length} migrations successfully reverted`)
        } else {
          print.warn('Nothing to revert')
        }
      })
    }

    function getCmd (name) {
      switch (name) {
        case 'status': return cmdStatus
        case 'up':
        case 'migrate': return cmdMigrate
        case 'down':
        case 'revert': return cmdRevert
        default:
          parameter = action
          return cmdMigrate
      }
    }

    const cmd = getCmd(action)

    return cmd(parameter).catch(err => {
      print.error(err)
    })
  })
}
