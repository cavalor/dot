import drop from './drop'
import migrate from './migrate'
import seed from './seed'

export default function (cli) {
  const {print} = cli
  print.subHeader('Drop database tables')
  return drop(cli).then(() => {
    print.subHeader('Start migrating database', {linesBefore: 1})
    return migrate(cli)
  }).then(() => {
    print.subHeader('Start seeding database', {linesBefore: 1})
    return seed(cli)
  })
}
