import fs from 'fs'
import path from 'path'
import faker from 'faker'
import { ensureDatabaseIsInitialized } from '../methods/db'

export default function (cli) {
  const { print, handleError } = cli
  const dbPath = cli.request.config.path
  const db = require(dbPath)
  const seedsPath = path.join(dbPath, 'seeds')

  function getSeedFileNames () {
    return fs.readdirSync(seedsPath)
  }

  return ensureDatabaseIsInitialized(dbPath)
    .then(getSeedFileNames)
    .then(async files => {
      if (!files.length) {
        return print.info('Nothing to process')
      } else {
        for (const file of files) {
          try {
            const seed = require(path.join(seedsPath, file))

            await seed(db, faker)
          } catch (err) {
            handleError(err)
          }
        }
        print.subHeader('Result')
        print.success(`Database seeded successfully`)
      }
    })
    .catch(handleError)
}
