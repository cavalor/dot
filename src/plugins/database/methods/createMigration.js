import fs from 'fs'
import path from 'path'
import { singular } from 'pluralize'
import { renderString } from 'nunjucks'
import { upperFirst, lowerFirst, camelCase } from 'lodash'

const templatePath = path.join(__dirname, '../templates/migration.nunj')

const entityName = str => upperFirst(camelCase(singular(str)))

export default function createMigration (cli) {
  const { print } = cli
  const { data } = cli.request
  const dbPath = cli.request.config.path

  let table = ''
  let fields = []

  if (data.target === 'relation') {
    const entities = [...data.entities]
      .map(e => entityName(e))
      .sort((a, b) => a > b)

    fields = entities.map(e => ({
      name: `${lowerFirst(e)}Id`,
      type: 'UUID',
      references: {
        model: e,
        key: 'id'
      },
      onUpdate: 'cascade',
      primaryKey: true
    }))

    for (const e of entities) {
      table = table.length ? `${table}_${e}` : e
    }
  } else if (data.target === 'entity') {
    table = entityName(data.entity)
    fields = data.fields
  } else {
    return Promise.reject(new Error(`Target type not supported: ${data.target}`))
  }

  if (data.timestamps) {
    fields.push(
      {name: 'createdAt', type: 'DATE'},
      {name: 'updatedAt', type: 'DATE'}
    )
  }

  if (data.softDelete) {
    fields.push({name: 'deletedAt', type: 'DATE'})
  }

  const fileName = `${Date.now()}_${data.action}_${table}.js`

  return new Promise((resolve, reject) => {
    try {
      // Nunjucks render doesn't work with config absolute path
      const template = fs.readFileSync(templatePath)
      const templateString = template.toString()

      renderString(templateString, {
        action: data.action,
        table,
        fields
      }, (err, compiledTemplate) => {
        if (err) return reject(err)

        const targetPath = path.join(dbPath, 'migrations', fileName)

        fs.writeFile(targetPath, compiledTemplate, (err) => {
          if (err) return reject(err)

          print.success(`Migration created: ${targetPath}`)

          resolve(targetPath)
        })
      })
    } catch (err) {
      reject(err)
    }
  })
}
