const fs = require('fs')
const path = require('path')
const {renderString} = require('nunjucks')
const {upperFirst, camelCase} = require('lodash')
const {singular} = require('pluralize')

const templatePath = path.join(__dirname, '../templates/model.nunj')

export default function createModel (cli) {
  const { print } = cli
  const { data } = cli.request
  const dbPath = cli.request.config.path

  return new Promise((resolve, reject) => {
    if (!data.target || data.target !== 'entity') return resolve()

    const modelName = upperFirst(camelCase(singular(data.entity)))
    const filePath = path.join(dbPath, 'models', `${modelName}.js`)
    const fileExist = fs.existsSync(filePath)

    if (fileExist) {
      print.info(`[skipped] File already exist: ${filePath}`)
      resolve()
    } else {
      try {
        // Nunjucks render doesn't work with config absolute path
        const file = fs.readFileSync(templatePath)
        const templateString = file.toString()

        renderString(templateString, {
          model: modelName,
          timestamps: data.timestamps,
          softDelete: data.softDelete,
          fields: data.fields
        }, (err, res) => {
          if (err) return reject(err)

          fs.writeFile(filePath, res, (err) => {
            if (err) return reject(err)

            print.success(`Model created: ${filePath}`)

            resolve(filePath)
          })
        })
      } catch (err) {
        reject(err)
      }
    }
  })
}
