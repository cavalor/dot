import { snakeCase, upperFirst, camelCase } from 'lodash'
import { renderString } from 'nunjucks'
import fs from 'fs'
import path from 'path'

const templatePath = path.join(__dirname, '../templates/seed.nunj')

export default function createSeed (cli) {
  const { print } = cli
  const { data } = cli.request
  const dbPath = cli.request.config.path

  return new Promise((resolve, reject) => {
    if (!data.seed) return resolve()

    const fileName = `${Date.now()}_${snakeCase(data.seed)}.js`
    const filePath = path.join(dbPath, 'seeds', fileName)
    const fileExist = fs.existsSync(filePath)

    if (fileExist) {
      print.info(`[skipped] File already exist: ${filePath}`)
      return resolve(filePath)
    }

    try {
      // Nunjucks render doesn't work with config absolute path
      const template = fs.readFileSync(templatePath).toString()

      renderString(template, {
        header: upperFirst(data.seed),
        model: upperFirst(camelCase(data.entity))
      }, (err, res) => {
        if (err) return reject(err)

        fs.writeFile(filePath, res, (err) => {
          if (err) return reject(err)

          print.success(`Seed created: ${filePath}`)

          resolve(filePath)
        })
      })
    } catch (err) {
      reject(err)
    }
  })
}
