import fs from 'fs'
import path from 'path'
import { ncp } from 'ncp'
import print from '../../../core/print'

export function databaseIsInitialized (dbPath) {
  return new Promise(resolve => {
    fs.stat(dbPath, (err) => {
      resolve(!err)
    })
  })
}

export function copyTemplateToDestPath (destPath) {
  const templatePath = path.join(__dirname, '../templates/db')

  return new Promise(resolve => {
    ncp(templatePath, destPath, (err) => {
      if (err) {
        throw err
      }

      fs.mkdirSync(path.join(destPath, 'migrations'))
      fs.mkdirSync(path.join(destPath, 'models'))
      fs.mkdirSync(path.join(destPath, 'seeds'))

      resolve()
    })
  })
}

export async function initializeDatabase (dbPath) {
  if (!dbPath) {
    throw Error('Database path argument is required')
  }

  const isInit = await databaseIsInitialized(dbPath)

  if (isInit) {
    print.warn('Database already initialized')

    return Promise.resolve()
  }

  await copyTemplateToDestPath(dbPath)

  print.success('Database initialized')
}

export async function ensureDatabaseIsInitialized (dbPath) {
  if (!dbPath) {
    throw Error('Database path argument is required')
  }

  const isInit = await databaseIsInitialized(dbPath)

  if (!isInit) {
    await initializeDatabase(dbPath)
  }
}
