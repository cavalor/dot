'use strict'

function env (name, defaultValue) {
  const val = process.env[name]
  return val !== undefined ? val : defaultValue
}

module.exports = {
  host: env('DB_HOST', '127.0.0.1'),
  database: env('DB_DATABASE', 'dev'),
  username: env('DB_USERNAME', 'dev'),
  password: env('DB_PASSWORD', null),
  dialect: env('DB_DIALECT', 'postgres')
}
