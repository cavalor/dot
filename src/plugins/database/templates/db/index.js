'use strict'

const fs = require('fs')
const path = require('path')
const Database = require('./Database')
const sequelize = require('./sequelize')

// Resolve entity descriptor file from filename
const resolve = file => path.resolve(__dirname, 'models', file)

// Read models directory and return each contained file name
const modelDescriptors = fs.readdirSync(path.join(__dirname, 'models'))

// Instantiate database api
const db = new Database(sequelize)

// Loop over model descriptors
for (const descriptor of modelDescriptors) {
  const model = sequelize.import(resolve(descriptor))
  db.setModel(model)
}

Object.values(db.models).map(model => {
  if (model.associate) {
    model.associate(db.models)
  }
})

// Export database as default
module.exports = db
